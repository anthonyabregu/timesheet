-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-11-2019 a las 18:19:23
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `picnic_webapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `act_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pry_id` int(11) NOT NULL,
  `act_descripcion` text NOT NULL,
  `act_tiempo` int(11) NOT NULL,
  `act_fecha` date NOT NULL,
  `act_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`act_id`, `usr_id`, `pry_id`, `act_descripcion`, `act_tiempo`, `act_fecha`, `act_registro`) VALUES
(1, 4, 4, 'Hola Mundo', 1, '2019-10-30', '2019-10-30 17:15:12'),
(2, 4, 4, 'Hola Mundo 2', 1, '2019-10-30', '2019-10-30 17:17:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `cln_id` int(11) NOT NULL,
  `cln_descripcion` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cln_id`, `cln_descripcion`) VALUES
(1, 'Eternit'),
(2, 'Menorca'),
(3, 'Pacasmayo'),
(4, 'Zalza'),
(5, 'Culqui'),
(6, 'La Ibérica'),
(7, 'Picnic');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE `presupuesto` (
  `prs_id` int(11) NOT NULL,
  `prs_descripcion` varchar(100) DEFAULT NULL,
  `prs_detalle` text,
  `prs_precio` float DEFAULT NULL,
  `prs_fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `cln_id` int(11) NOT NULL,
  `pry_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`prs_id`, `prs_descripcion`, `prs_detalle`, `prs_precio`, `prs_fecha`, `cln_id`, `pry_id`) VALUES
(1, 'Presupuesto 1', '<p><b>Este es el presupuesto 1</b></p>', 1200.1, '2019-10-30 12:30:43', 1, 1),
(2, 'Presupuesto 2', '<p><b>Hola, este es el presupuesto 2, editado</b></p>', 1356.23, '2019-10-30 16:26:34', 1, 2),
(3, 'Presupuesto 3', '<p><b>Hola amigo, este es el presupuesto 3</b></p>', 1500.45, '2019-10-30 16:32:30', 1, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `pry_id` int(11) NOT NULL,
  `pry_descripcion` varchar(70) NOT NULL,
  `cln_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`pry_id`, `pry_descripcion`, `cln_id`) VALUES
(1, 'Construye con Drywall', 1),
(2, 'Constructores del Futuro', 1),
(3, 'Website', 2),
(4, 'Canal Online', 2),
(7, 'Messenger Bot', 2),
(8, 'Emailing', 2),
(9, 'Otros', 2),
(10, 'Webapp', 3),
(11, 'Landing mPOS', 5),
(12, 'Encuestas', 2),
(13, 'Otros', 1),
(14, 'Nueva web', 7),
(15, 'Milky', 6),
(16, 'Webapp', 4),
(17, 'Precios Pacasmayo', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usr_id` int(11) NOT NULL,
  `usr_nombre` varchar(70) NOT NULL,
  `usr_email` varchar(70) NOT NULL,
  `usr_pass` varchar(10) NOT NULL,
  `usr_tipo` int(11) NOT NULL,
  `usr_estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usr_id`, `usr_nombre`, `usr_email`, `usr_pass`, `usr_tipo`, `usr_estado`) VALUES
(1, 'Jonatán López', 'jonatan@picnic.pe', '1234', 1, 1),
(2, 'Jonathan Peralta', 'josue@picnic.pe', '1234', 2, 1),
(3, 'Javier Díaz', 'javier@picnic.pe', '1234', 2, 1),
(4, 'Anthony Abregu', 'anthony@picnic.pe', '1234', 1, 1),
(5, 'Enrique Higueras', 'enrique@picnic.pe', '1234', 1, 1),
(6, 'Hugo Campodónico', 'hugo@picnic.pe', '1234', 1, 1),
(7, 'Alex Spelucin', 'alex@picnic.pe', '1234', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`act_id`),
  ADD KEY `fk_actividad_usuario` (`usr_id`),
  ADD KEY `fk_actividad_proyecto` (`pry_id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cln_id`);

--
-- Indices de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`prs_id`),
  ADD KEY `fk_pres_cliente` (`cln_id`),
  ADD KEY `fk_pres_proyecto` (`pry_id`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`pry_id`),
  ADD KEY `fk_cliente_proyecto` (`cln_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usr_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `act_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cln_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `prs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `pry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `fk_actividad_proyecto` FOREIGN KEY (`pry_id`) REFERENCES `proyecto` (`pry_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_actividad_usuario` FOREIGN KEY (`usr_id`) REFERENCES `usuario` (`usr_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `fk_pres_cliente` FOREIGN KEY (`cln_id`) REFERENCES `cliente` (`cln_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pres_proyecto` FOREIGN KEY (`pry_id`) REFERENCES `proyecto` (`pry_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_cliente_proyecto` FOREIGN KEY (`cln_id`) REFERENCES `cliente` (`cln_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
