$(document).ready(function () {

    listar();
    //listarContacto();

    $(".filtros select[name=proyecto]").bind("change", function () {
        listar();
    });

    /*new Request("proyecto/listar/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id_web+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });*/

    new Request("proyecto/listar_proyectos/",{
        estado: 1,
        usr_id: idUsuario,
        usr_tipo: tipoUsuario,
        rol: 3
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         
    });

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                listar(1);
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});



function listar() {
    console.log('log');
    $("#referido .lista").empty();

    var pry_id = $(".filtros select[name=proyecto]").val();

    new Request("referido/listareferidos/", {
        pry_id: pry_id
    }, function (res) {
        $("#referido .lista").empty();

        if(res.res == "ok"){

            $.each(res.lista, function (k, v) {
                var it = new ItemReferido(v);
                $("#referido .lista").append(it);
            })

        }else{
            alert(res.res);
        }
        

    });


}


var ItemReferido = function (data) {

    /*REFERIDOS*/
    var idref = data.rfn_id;
    var nombre = data.rfn_nombres;
    var celular = data.rfn_celular;
    var email = data.rfn_email;
    var tiporef = data.rfn_tipo_documento;
    var dni = data.rfn_n_documento;

    if(tiporef == 1){tiporef = "Dni";}
    if(tiporef == 2){tiporef = "Carnet de Extranjería";}

    /*ORIGEN*/

    var orf_id = data.orf_id;
    var orf_nombre = data.orf_nombre;
    var tipo_orf = data.orf_tipo_documento;
    var orf_dni = data.orf_n_documento;
    var orf_celular = data.orf_celular;
    var orf_email = data.orf_email;

    if(tipo_orf == 1){tipo_orf = "Dni";}
    if(tipo_orf == 2){tipo_orf = "Carnet de Extranjería";}

    /* PROYECTO */

    var proyecto = data.pry_descripcion;

    var html = $('<tr width="100%">' +
        '<td width="10%" style="vertical-align:middle;">' + idref + '</td>' +
        '<td width="20%" style="vertical-align:middle;">' + nombre + '</td>' +
        '<td width="20%" style="vertical-align:middle;">' + email + '</td>' +
        '<td width="10%" style="vertical-align:middle;">' + celular + '</td>' +
        '<td width="20%" style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td width="20%"><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar"><i class="fas fa-eye"></i> Ver</button>' +
        '<button type="button" class="btn btn-primary btn-origen"><i class="fas fa-eye"></i> Origen</button>' +

        '</div></td>' +
        '</tr>');

        /*    var html = $('<tr width="100%" data-id="' + id + '">' +
                    '<td>' + data.pry_titulo + '</td>' +
                    '<td>' + data.pry_orden + '</td>' +
                    '<td><div class="btn-group" role="group" aria-label="...">' +
                    '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
                    '</div></td>' +
                '</tr>');*/



        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(idref);
            $("#modaleditar input[name=nombre]").val(nombre);
            $("#modaleditar input[name=tipo_documento]").val(tiporef);
            $("#modaleditar input[name=dni]").val(dni);
            $("#modaleditar input[name=email]").val(email);
            $("#modaleditar input[name=celular]").val(celular);
            $("#modaleditar input[name=proyecto]").val(proyecto);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });

        html.find(".btn-origen").click(function () {

            $("#modalorigen input[name=id]").val(orf_id);
            $("#modalorigen input[name=nombre]").val(orf_nombre);
            $("#modalorigen input[name=tipo_documento]").val(tipo_orf);
            $("#modalorigen input[name=dni]").val(orf_dni);
            $("#modalorigen input[name=email]").val(orf_email);
            $("#modalorigen input[name=celular]").val(orf_celular);

            $("#modalorigen").modal("show");

            $("#modalorigen a.eliminar").unbind();
            $("#modalorigen a.eliminar").click(function () {
                $("#modalorigen").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });


    return html;
};


