$(document).ready(function () {

    $(".modal input[name=referenciado]").val(referenciado);

    listar();
    
    

    /*$(".filtros select[name=estado]").bind("change", function () {
        var est = $(this).val();
        listar(est);
    });*/

    

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                $(".modal input[name=referenciado]").val(referenciado);
                proyectos();
                listar(1);
            }else{
                alert(res.res);
                $("#modalagregar").modal("hide");
                $(".modal input[name=referenciado]").val(referenciado);
                proyectos();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});


/*function proyectos(){
    new Request("proyecto/listar_proyectos/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });
}*/



function listar() {
    console.log('log');
    $("#designar .lista").empty();

    new Request("referido/referenciados/", {
        orf_id : referenciado
    }, function (res) {
        $("#referenciado .lista").empty();
        if(res.res == "ok"){

            $.each(res.lista, function (k, v) {
                var it = new ItemReferenciado(v);
                $("#referenciado .lista").append(it);
            })
        }else{
            alert(res.res);
        }
        
    });
}


var ItemReferenciado = function (data) {

	var id = data.rfn_id;
    var nombre = data.rfn_nombres;
    var celular = data.rfn_celular;
    var email = data.rfn_email;

    var tipodocumento = data.rfn_tipo_documento;
    var documento = data.rfn_n_documento;

    if(tipodocumento == 1){
        tipodocumento = "Dni";
    }else{
        tipodocumento = "Carnet de Extranjería";
    }

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td style="vertical-align:middle;">' + celular + '</td>' +
        '<td style="vertical-align:middle;">' + email + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-editar" style="margin-right: 5px;" ><i class="fa fa-edit"></i></button>' +

        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

        	$("#modaleditar input[name=referenciado]").val(referenciado);
        	$("#modaleditar input[name=nombre]").val(nombre);
            $("#modaleditar input[name=documento]").val(tipodocumento + ' / ' + documento);
            $("#modaleditar input[name=celular]").val(celular);
            $("#modaleditar input[name=email]").val(email);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("designarwsp/eliminar/" + usw_id, {
                    }, function (res) {
                        listar(1);
                        $("#modaleliminar").modal("hide");
                    });
                });

            })

            


        });

    return html;
};
