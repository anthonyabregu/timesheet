$(document).ready(function () {
  
//dashboard();

$('input[name="daterange"]').daterangepicker({
        opens: 'right',
        //"autoUpdateInput": false,
        "showCustomRangeLabel": false,
         "alwaysShowCalendars": true,
         "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Seleccionar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "hasta",
            "customRangeLabel": "Personalizado",
            "weekLabel": "W",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
      ranges: {
             'Hoy': [moment(), moment()],
             'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Últimos 7 Days': [moment().subtract(6, 'days'), moment()],
             'Últimos 30 Days': [moment().subtract(29, 'days'), moment()],
             'Este mes': [moment().startOf('month'), moment().endOf('month')],
             'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });


      $("#btn-reporte").click(function(){

          var nombreGraph = "Gráfica de Contactos por Proyecto";
          var contenedor = "graph-contacto";

          $("#graph-contacto").empty();

          var rango = $("input[name=daterange]").val();
          
          new Request("contacto/dashboard_contacto/", {
              rango:rango
          },function(res){

              var data = res.lista;
              new Pie(contenedor,data,nombreGraph);

              $(".lista_contacto").empty();

              $.each(data,function(k,v){
                var it = new ItemContacto_P(v);
                $(".lista_contacto").append(it);
              })

            });
      })

});



function dashboard() {
    console.log('log');
    $("#contacto .lista").empty();

    var nombreGraph = "Gráfica de Contactos por Proyecto";
    var contenedor = "graph-contacto";

    new Request("contacto/dashboard_contacto/", {

    }, function (res) {
        console.log(res);
        if(res.res =="ok"){

          var data = res.lista;
          new Pie(contenedor,data,nombreGraph);

          $(".lista_contacto").empty();
          
          $.each(res.lista, function (k, v) {
            var it = new ItemContacto_P(v);
            $(".lista_contacto").append(it);
            
          })
        }
    });
}


var ItemContacto_P = function (data) {

    var proyecto = data.proyecto;
    var cantidad = data.cantidad;
    var total = data.total;

    var porcentaje = (cantidad/total)*100;

    //Math.round(1.005*100)/100;

    //porcentaje = Math.round(porcentaje*100)/100;

    porcentaje = Math.round(porcentaje);
    

    var html = $('<tr width="100%">' +
                    '<td style="vertical-align:middle;">' + proyecto + '</td>' +
                    '<td style="vertical-align:middle;">' + cantidad + '</td>' +
                    '<td style="vertical-align:middle;">' + porcentaje+ '%' + '</td>' +
                  '</tr>');


    

    //new Pie(html.find(".graph-contacto"),data,nombreGraph);

    return html;
};


var Pie = function(contenedor,data,titulo){


    console.log(data);

    $("#"+ contenedor).css("height",400);

    var datos = new Array();

    $.each(data,function(k,v){
        datos.push({name:v.proyecto,y:parseInt(v.cantidad)});
    });

    console.log(datos);

    Highcharts.chart(contenedor, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: titulo
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.0f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          }
        }
      },
      series: [{
        name: 'Valor',
        colorByPoint: true,
        data: datos
      }]
    });
}


