var categorias;
var index_foto = 1;
var index_act = 1;

$(document).ready(function () {

  listar();

  new Request("cliente/listar/",{
        
  },function(res){
        console.log(res);
        $.each(res.lista,function(k,v){
            var option = '<option value="'+v.cln_id+'">'+v.cln_descripcion+'</option>'
            $("select[name=cliente]").append(option);
        });

         
    });

    $(".filtros select[name=cliente]").bind("change",function(){
        
        listar();

    })

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modalagregar").modal("hide");
                //$("#modalagregar select[name=proyecto]").val("");
                listar();

            }else{
              alert("No se pudo agregar el proyecto");
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modaleditar").modal("hide");
                listar();
                totalHoras();

            }else{
                alert("No se pudo editar el proyecto");
            }
        }
    }

    $("#modaleditar form").ajaxForm(options);

});


function listar(){

  var cliente = $(".filtros select[name=cliente]").val();

  $("#proyecto .lista").empty();

  new Request("proyecto/listar/", {
        cliente: cliente
    }, function (res) {
        //console.log(res);
        $("#proyecto .lista").empty();
        if(res.res == "ok"){
          $.each(res.lista, function (k, v) {
              var it = new ItemProyecto(v);
              $("#proyecto .lista").append(it);
          })
        }else{
          alert("No hay registros");
        }
    });
}

var ItemProyecto = function (data) {

    var id = data.pry_id;
    var nombrecliente = data.cln_descripcion;
    var nombreproyecto = data.pry_descripcion;
    var idcliente = data.cln_id;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombreproyecto + '</td>' +
        '<td style="vertical-align:middle;">' + nombrecliente + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-sm btn-editar" title="Editar Horas"><i class="fa fa-edit"></i></button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-eliminar" ><i class="fa fa-minus-circle"></i></button>' +
        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombre]").val(nombreproyecto);
            $("#modaleditar select[name=cliente]").val(idcliente);
            $("#modaleditar").modal("show");

            /*$("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })*/


        });

        html.find(".btn-eliminar").click(function () {

            $("#modaleliminar").modal("show");
            $("#modaleliminar .id").html(id);


            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {
                $("#modaleliminar").modal("hide");
                new Request("proyecto/eliminar/" + id, {
                }, function (res) {
                    //listar();
                    html.remove();
                });
               
            })


        });

    return html;
};


