$(document).ready(function () {

	listar();

	/*new Request("proyecto/listar/",{
        
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.cln_descripcion+' - '+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         
    });*/

    /*$(".filtros input[name=fecha]").bind("change",function(){
        var fecha = $(this).val();
        //alert(fecha);
        listar();
        totalHoras();

    })*/

    /*$(".filtros select[name=proyecto]").bind("change",function(){
        
        listar();
        totalHoras();

    })*/


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modalagregar").modal("hide");
                //$("#modalagregar select[name=proyecto]").val("");
                listar();

            }else{
            	alert("No se pudo agregar la actividad");
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modaleditar").modal("hide");
                listar();

            }else{
                alert("No se pudo editar el cliente");
            }
        }
    }

    $("#modaleditar form").ajaxForm(options);

});


function listar() {
    
    //var fecha = $(".filtros input[name=fecha]").val();
    //var proyecto = $(".filtros select[name=proyecto]").val();

    $("#cliente .lista").empty();

    new Request("cliente/listar/", {
        
    }, function (res) {
        console.log(res);
        $("#cliente .lista").empty();
        if(res.res == "ok"){
        	$.each(res.lista, function (k, v) {
	            var it = new ItemCliente(v);
	            $("#cliente .lista").append(it);
        	})
        }else{
        	alert("No hay registros");
        }
        
            

    });


}



var ItemCliente = function (data) {

    var id = data.cln_id;
    var nombre = data.cln_descripcion;
    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-sm btn-editar"><i class="fa fa-edit"></i></button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-eliminar" ><i class="fa fa-minus-circle"></i></button>' +
        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=nombre]").val(nombre);

            $("#modaleditar").modal("show");

            /*$("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })*/


        });

        html.find(".btn-eliminar").click(function () {

            $("#modaleliminar").modal("show");
            $("#modaleliminar .id").html(id);


            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {
                $("#modaleliminar").modal("hide");
                new Request("cliente/eliminar/" + id, {
                }, function (res) {
                    //listar();
                    html.remove();
                });
               
            })


        });

    return html;
};


