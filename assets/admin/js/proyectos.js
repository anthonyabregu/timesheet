var categorias;
var index_foto = 1;
var index_act = 1;

$(document).ready(function () {

    $(".filtros select[name=categoria]").bind("change",listar);

    $("#proyectos .filtros select[name=estado]").bind("change",listar);

    new Request("categoria/listar/",{
        estado: 1,
        categoria:1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.ctg_id+'">'+v.ctg_nombre+'</option>'
            $("select[name=categoria]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         listar();
    });

    /* envia form del modalagregar*/
      var options = {
    		dataType: 'json',
    		type: 'post',
    		clearForm:true,
    		success: function(res){
    			if(res.res=="ok"){
            $("#modalagregar .fotos-referenciales").empty();
		        $("#modalagregar").modal("hide");
            index_foto = 1;
    				listar();
    			}
    		}
    	}
      $("#modalagregar form").ajaxForm(options);

      /* envia form del modaleditar*/
        var options = {
    		dataType: 'json',
    		type: 'post',
    		clearForm:true,
    		success: function(res){
    			if(res.res=="ok"){
            console.log(res);
            $("#modaleditar").modal("hide");
            $("#modaleditar .mostrar-fr").attr('disabled',false);
    				listar();
    			}
    		},
        error: function(res){
          console.log('log');
          console.log(res);
        }
    	}
      $("#modaleditar form").ajaxForm(options);

      var options = {
      dataType: 'json',
      type: 'post',
      clearForm:true,
      success: function(res){
        if(res.res=="ok"){ console.log(res);
          $("#editarfoto").modal("hide");
          $('#modaleditar').modal('show');
        }
      }
    }
    $("#editarfoto form").ajaxForm(options);


    /**/
    //ORDENAR LISTA Fotos
    $("#modalordenar .lista" ).sortable({
		items: "> tr",
		opacity: 0.7,
		update:function(event,ui){

			// $(".orden").show();

		}
	});

  //ORDENAR proyectos tabla
  $("#proyectos .lista" ).sortable({
  items: "> tr",
  opacity: 0.7,
  update:function(event,ui){

    $(".orden").show();

  }
});

	$("#proyectos .orden .btn-guardar").click(function(){
		var ids = $( "#proyectos .lista").sortable( "toArray",{attribute:"data-id"} );
		new Request("proyecto/ordenar",{
			"lista":ids.join(",")
		},function(res){
      console.log(res);
			listar();
			console.log("orden guardado");
		});
	});

  $("#proyectos .orden .btn-cancelar").click(function(){
		$(".orden").hide();
		$("#proyectos .lista").empty();
		listar();
	});

  $("#modalordenar .btn-guardar").click(function(){
      var ids = $( "#modalordenar .lista").sortable( "toArray",{attribute:"data-id"} );
      new Request("foto/ordenar",{
        "lista":ids.join(",")
      },function(res){
          if(res.res == 'ok'){
            $('#modalordenar').modal('hide');
            $('#modaleditar').modal('show');
          }
      });
  });

  $("#modalordenar .btn-cancelar").click(function(){
    $("#modalordenar").hide();
    $('#modaleditar').show();
  });



  $("#modalagregar .ordenar-f").click(function(){
      $("#modalagregar").modal("hide");
  })

  $("#modaleditar .ordenar-f").click(function(){
      $("#modaleditar").modal("hide");
  })

  $('#modalordenar button').click(function(){
    $("#modaleditar").modal("show");
  });

  $("#modaleditar .ordenar-f").click(function(){

      var pry_id = $('#modaleditar input[name=id]').val();
      console.log(pry_id);
      new Request("foto/listar", {
        id:pry_id
      }, function (res) {
        $("#modalordenar .lista").empty();
      $.each(res, function (k, v) {
          var it = new ItemFoto_ordenar(v);
          $("#modalordenar .lista").append(it);
          index_act++;
        });
      });
  });


  $('#editarfoto button[type=button]').click(function(){
    $("#modaleditar").modal("show");
  });

  $("#modalagregar .agregar-fr").click(function(){
      var form_foto = `<div class="form-group" id="foto_div${index_foto}">
                            <div class="card">
                              <div class="card-body">
                                <label>Foto #<span>${index_foto}</span></label>
                                <input type="file" ord="${index_foto}" name="foto[]" accept=".jpg,.jpeg"  class="form-control-file" required>
                                <br>
                                <select name="estado_fr[]" class="form-control" required>
                                    <option value="1" selected>Habilitado</option>
                                    <option value="0">Deshabilitado</option>
                                </select>
                                <br>
                                <button type="button" btn-id=${index_foto} class="btn btn-danger btn-sm quitar-fr">Quitar</button>
                              </div>
                            </div>
                            <br>
                        </div>`;

       $('.fotos-referenciales').append(form_foto);
      index_foto++;
  });

  $("#modaleditar .agregar-fr").click(function(){
      var form_foto = `<div class="form-group" id="foto_div${index_act}">
                            <div class="card">
                              <div class="card-body">
                                <input type="file" name="foto[]" accept=".jpg,.jpeg"  class="form-control-file" required>
                                <br>
                                <select name="estado_fr[]" class="form-control" required>
                                    <option value="1" selected>Habilitado</option>
                                    <option value="0">Deshabilitado</option>
                                </select>
                                <br>
                                <button type="button" btn-id=${index_act} class="btn btn-danger btn-sm quitar-fr">Quitar</button>
                              </div>
                            </div>
                            <br>
                        </div>`;

       $('#modaleditar .fotos-referenciales').append(form_foto);
      index_act++;
  });

  $("#modalagregar .fotos-referenciales").on("click", `.quitar-fr`, function() {

      var id = $(this).attr('btn-id');
      $(`.fotos-referenciales #foto_div${id}`).remove();
      if(!$("#modalagregar .fotos-referenciales #foto_div1").length)index_foto=1;
      console.log();
    });

    $("#modaleditar .fotos-referenciales").on("click", `.quitar-fr`, function() {

        var id = $(this).attr('btn-id');
        $(`.fotos-referenciales #foto_div${id}`).remove();
        if(!$("#modalagregar .fotos-referenciales #foto_div1").length)index_act=1;
        console.log();
      });


    $("#modaleditar .mostrar-fr").click(function(){
      var pry_id = $('#modaleditar input[name=id]').val();
      console.log(pry_id);
      new Request("foto/listar", {
          id:pry_id
      }, function (res) {
              $.each(res, function (k, v) {
                  var it = new ItemFoto(v,index_act);
                  $("#modaleditar .fotos-referenciales").append(it);
                  index_act++;
              });
      });
      $(this).attr('disabled',true);
    });


    $("#modaleditar .dismiss").click(function(){
         $('#modaleditar .fotos-referenciales').empty();
         $("#modaleditar .mostrar-fr").attr('disabled',false);
        listar();
    })
});



/*filtro*/
function listar() {
    $("#proyectos .lista").empty();
    $(".orden").hide();

    var ctg = $(".filtros select[name=categoria]").val();
    var es = $(".filtros select[name=estado]").val();
    new Request("proyecto/listar", {
        estado: es,
        categoria: ctg
    }, function (res) {
          if(res.res != 'empty'){
            $("#proyectos .lista").empty();
            $.each(res, function (k, v) {

                var it = new ItemProyecto(v);
                $("#proyectos .lista").append(it);
            })
          }
          else{
            alert('No hay proyectos en esta categoria y estado')
          }
    });
    $("#modalagregar select[name=categoria]").val(ctg);
}


var ItemFoto = function(data,index_act){
console.log(data);
  var src = path+'files/proyectos/'+data.fto_archivo;

  var form_foto = $(`<div class="form-group" id="foto_div${index_act}">
                        <div class="card">
                          <div class="card-body">
                            <label>Preview Foto (Referencial)</label>
                            <br>
                            <img src="${src}" class="img-thumbnail foto" >
                            <br><br>
                            <button type="button" class="btn btn-primary btn-sm editar-fr" data-toggle="modal" data-target="#editarfoto">Cambiar Foto</button>
                            <button type="button" btn-id=${index_act} class="btn btn-danger btn-sm quitar-fr">Quitar</button>
                          </div>
                        </div>
                        <br>
                    </div>`);

    form_foto.find('.quitar-fr').click(function(){
        new Request("foto/eliminar/", {id:data.fto_id}, function (data) {
              console.log(data);
        });

    });

    form_foto.find('.editar-fr').click(function(){

        $('#editarfoto input[type=hidden]').val(data.fto_id);
        $('#editarfoto #pry_id').val(data.pry_id);
        $("#editarfoto select[name=estado_act]").val(data.fto_estado);
        $('#modaleditar').modal('hide');
    });

    return form_foto;
}

var ItemFoto_ordenar = function (data) {

    var foto_id = data.fto_id;

    var html = $(`<tr width="100%" data-id="${foto_id}">
        <td><img src="${path}files/proyectos/${data.fto_archivo}" width="50"></td>
        <td>${data.fto_archivo}</td>
        <td>${data.fto_orden}</td>
        </tr>`);

     return html;
}


var ItemProyecto = function (data) {
    // console.log(data);
    var proyecto_id = data.pry_id;

    var html = $('<tr width="100%" data-id="' + proyecto_id + '">' +
        '<td><img src="' + path+'files/proyectos/'+data.pry_imagen + '" width="50"></td>' +
        '<td>' + data.pry_titulo + '</td>' +
        '<td>' + data.pry_orden + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {
      console.log(data);
      $("#modaleditar .fotos-referenciales").empty();
      $("#modaleditar input[name=id]").val(data.pry_id);
      $("#modaleditar select[name=categoria]").val(data.ctg_id);
      $("#modaleditar input[name=titulo]").val(data.pry_titulo);
      $("#modaleditar .foto").attr("src","");
      $("#modaleditar .foto").attr("src",path+"files/proyectos/"+data.pry_imagen);


      if(data.pry_espacios != '' || data.pry_espacios != null ){
          var split_espacios = data.pry_espacios.split(',');

          for (var i = 0; i < split_espacios.length; i++) {
            $(`#modaleditar .espacios input[value=${split_espacios[i]}]`).prop('checked', true);
          }
      }

      if(data.pry_otrosusos != null || data.pry_otrosusos != ''){
          var split_ousos = data.pry_otrosusos.split(',');
          for (var i = 0; i < split_ousos.length; i++) {
            $(`#modaleditar .otros_usos input[value=${split_ousos[i]}]`).prop('checked', true);
          }
      }

      $("#modaleditar img.foto_p").attr("src","");
      $("#modaleditar img.foto_p").attr("src",path+"files/proyectos/"+data.pry_plano1);

      $("#modaleditar img.foto_s").attr("src","");

      if(data.pry_plano2 !=null)
        $("#modaleditar img.foto_s").attr("src",path+"files/proyectos/"+data.pry_plano2);
      else{
        $("#modaleditar img.foto_s").attr("alt",'La imagen no fue registrada');
      }

      if(data.pry_pdf == null){
        $("#modaleditar .mensaje").html("<span>El pdf no fue registrado</span>");
      }
      else{
        $("#modaleditar .mensaje").html(`<h2>${data.pry_pdf}</h2>`);
      }
      $("#modaleditar select[name=destacado]").val(data.pry_destacado);
      $("#modaleditar select[name=estado]").val(data.pry_estado);

      $("#modaleditar").modal("show");

      $("#modaleditar a.eliminar").unbind();
      $("#modaleditar a.eliminar").click(function () {
          $("#modaleditar").modal("hide");
          $("#modaleliminar .btn-eliminar").unbind();
          $("#modaleliminar .btn-eliminar").click(function () {

              new Request("proyecto/eliminar/",{id:data.pry_id} , function (data) {
                  listar();
                  $("#modaleliminar").modal("hide");
              });
          });

      })

    });

    return html;

};
