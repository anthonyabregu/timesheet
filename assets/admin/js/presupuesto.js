$(document).ready(function () {

    /*$('#summernote').summernote({
      height: 300,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      focus: true                  // set focus to editable area after initializing summernote
    });*/

	listar();

    //listarproyecto("");

    var url = "http://localhost:8080/timesheet/";

    /*$.ajax({
        url: url + "cliente/listar/",
        dataType: "json",
        data: "",
        type: "GET",
        success: function (res) {
            
            console.log(res);
            $.each(res.lista,function(k,v){
                var option = '<option value="'+v.cln_id+'">'+v.cln_descripcion+'</option>'
                $("select[name=cliente]").append(option);
                // $("#modalagregar #cat_add select[name=categoria]").append(option);
            });

        }
    });*/
	
    new Request("cliente/listar/",{
        
    },function(res){
        console.log(res);
        $.each(res.lista,function(k,v){
            var option = '<option value="'+v.cln_id+'">'+v.cln_descripcion+'</option>'
            $("select[name=cliente]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         
    });

    $(".filtros input[name=fecha]").bind("change",function(){
        var fecha = $(this).val();
        //alert(fecha);
        listar();
        //totalHoras();

    })

    $(".filtros select[name=proyecto]").bind("change",function(){
        
        listar();

    })

    $(".filtros select[name=cliente]").bind("change",function(){

        var cliente = $(this).val();
        
        listar();
        listarproyecto(cliente);

    })

    $("#modalagregar select[name=cliente]").bind("change",function(){

        var cliente = $(this).val();
        
        listarproyecto(cliente);

    })

    $("#modaleditar select[name=cliente]").bind("change",function(){

        var cliente = $(this).val();
        
        listarproyecto(cliente);

    })

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            console.log(res);
            if (res.res == "ok") {

                $("#modalagregar").modal("hide");
                $("#modalagregar select[name=proyecto]").val("");
                $("#modalagregar .summernote").summernote("code", "");
                listar();

            }else{
            	alert("No se pudo agregar el presupuesto");
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modaleditar").modal("hide");
                listar();
                //totalHoras();

            }else{
                alert("No se pudo editar el presupuesto");
            }
        }
    }

    $("#modaleditar form").ajaxForm(options);

});


function listar() {
    
    var fecha = $(".filtros input[name=fecha]").val();
    var proyecto = $(".filtros select[name=proyecto]").val();
    var cliente = $(".filtros select[name=cliente]").val();

    $("#actividad .lista").empty();

    new Request("presupuesto/listar/", {
        cliente: cliente,
        proyecto: proyecto,
        fecha: fecha
    }, function (res) {
        console.log(res);
        $("#actividad .lista").empty();
        if(res.res == "ok"){
        	$.each(res.lista, function (k, v) {
	            var it = new ItemActividad(v);
	            $("#actividad .lista").append(it);
        	})
        }else{
        	alert("No hay registros");
        }
        
            

    });


}



var ItemActividad = function (data) {

    var id = data.prs_id;
    var descripcion = data.prs_descripcion;
    var fecha = data.prs_fecha;
    var detalle = data.prs_detalle;
    var precio = data.prs_precio;
    var idcliente = data.cln_id;
    var idproyecto = data.pry_id;
    var proyecto = data.pry_descripcion;
    var cliente = data.cln_descripcion;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + fecha + '</td>' +
        '<td style="vertical-align:middle;">' + descripcion + '</td>' +
        '<td style="vertical-align:middle;">' + cliente + '</td>' +
        '<td style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-sm btn-editar" title="Editar Horas"><i class="fa fa-edit"></i></button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-eliminar" ><i class="fa fa-minus-circle"></i></button>' +
        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {
            
            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=descripcion]").val(descripcion);
            $("#modaleditar select[name=cliente]").val(idcliente);
            //listarproyecto(idcliente, "e");
            $("#modaleditar select[name=proyecto]").val(idproyecto);
            $("#modaleditar textarea[name=detalle]").val(detalle);
            $("#modaleditar .summernote").summernote('code',detalle); //add text to summernote
            $("#modaleditar input[name=precio]").val(precio);
            $("#modaleditar").modal("show");

            /*$("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })*/


        });

        html.find(".btn-eliminar").click(function () {

            $("#modaleliminar").modal("show");
            $("#modaleliminar .id").html(id);


            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {
                $("#modaleliminar").modal("hide");
                new Request("presupuesto/eliminar/" + id, {
                }, function (res) {
                    //listar();
                    html.remove();
                });
               
            })


        });

    return html;
};


function listarproyecto(cliente){

    //var cliente = $(".filtros select[name=cliente]").val();

    var url = "http://localhost:8080/timesheet/";

  /*$.ajax({
        url: url + "proyecto/listar/",
        dataType: "json",
        data: {cliente: cliente},
        type: "GET",
        success: function (res) {
            
            $("select[name=proyecto]").html("");
            console.log(res);
            $("select[name=proyecto]").append("<option value=''>Seleccione...</option>");
            $.each(res.lista,function(k,v){
                var option = '<option value="'+v.pry_id+'">'+v.pry_descripcion+'</option>'
                $("select[name=proyecto]").append(option);
            });

        }
    });*/

    new Request("proyecto/listar/",{
        cliente: cliente
    },function(res){
        $("select[name=proyecto]").html("");
        console.log(res);
        $("select[name=proyecto]").append("<option value=''>Seleccione...</option>");
        $.each(res.lista,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.cln_descripcion+' - '+v.pry_descripcion+'</option>';
            $("select[name=proyecto]").append(option);
            
        });

         
    });

}


