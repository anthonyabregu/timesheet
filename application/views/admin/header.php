<?php 
    $tipo = $this->session->userdata('tipo');
    $idUsuario = $this->session->userdata('id');
    $nombreUsuario = $this->session->userdata('nombre');
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Picnic | WebApp</title>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/bootstrap/css/bootstrap.min.css"> -->
     

        <!-- include summernote css/js -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">


        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/summernote/summernote.css"> -->
        <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script> -->
        
        <!-- <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script> -->

        <!-- <script src="<?php echo base_url(); ?>assets/libs/jquery-3.2.1.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/admin/js/popper.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/libs/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

    <!-- include summernote css/js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    <script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js?v=2"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


    <!-- Comentado para que el summernote funcione -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> -->

    <script src="<?php echo base_url();?>assets/web/js/ubigeo.js?v=2"></script>

    <!-- Datatables -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/libs/DataTables/datatables.js"></script> -->
    <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->

    
    <script src="<?php echo base_url();?>assets/admin/js/<?= $page ?>.js"></script>

    <script src="<?php echo base_url();?>assets/web/js/home.js"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>  -->
    <!-- <script src="<?php echo base_url(); ?>assets/libs/summernote/summernote.js"></script> -->
    <!--<script src="<?php echo base_url(); ?>assets/libs/summernote/lang/summernote-es-EU.js"></script> -->

    
    

    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>assets/libs/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url();?>assets/libs/jquery/jquery.form.js"></script>

    <script src="<?php echo base_url();?>assets/libs/jquery/jquery-ui.js"></script> -->


        <style>

          #cargando{
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0px;
            display: none;
            left: 0px;
            background-color: rgba(0,0,0,0.7);
            z-index: 9999;
          }
          #cargando .table{
            width: 100%;
            height: 100%;
            display: table;
          }
          #cargando .table .cell{
            width: 100%;
            height: 100%;
            display: table-cell;
            vertical-align: middle;
          }
          .spinner {
            width: 40px;
            height: 40px;

            position: relative;
            margin: 100px auto;
          }

          .double-bounce1, .double-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #fff;
            opacity: 0.6;
            position: absolute;
            top: 0;
            left: 0;

            -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
            animation: sk-bounce 2.0s infinite ease-in-out;
          }

          .double-bounce2 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
          }

          @-webkit-keyframes sk-bounce {
            0%, 100% { -webkit-transform: scale(0.0) }
            50% { -webkit-transform: scale(1.0) }
          }

          @keyframes sk-bounce {
            0%, 100% {
              transform: scale(0.0);
              -webkit-transform: scale(0.0);
            } 50% {
              transform: scale(1.0);
              -webkit-transform: scale(1.0);
            }
          }

        </style>

    </head>
    <body>

        <div class="modal" tabindex="-1" role="dialog" id="modalalerta">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
              </div>
            </div>
          </div>
        </div>

        <!-- <div id="cargando">
          <div class="table">
            <div class="cell">
              <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
              </div>
            </div>
          </div> -->

        </div>
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="<?php echo base_url();?>admin/">Picnic WebApp</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">

                            <?php if($tipo == 1){ ?>
                
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url();?>admin/actividad">Actividad<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url();?>admin/cliente">Clientes<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url();?>admin/proyecto">Proyectos<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url();?>admin/presupuesto">Presupuesto<span class="sr-only">(current)</span></a>
                                </li>

                            <?php } else{ ?>


                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url();?>admin/actividad">Actividad<span class="sr-only">(current)</span></a>
                                </li>

                            <?php } ?>
                            <!---<li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url();?>admin/proyectos">Proyectos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url();?>admin/tipo_proyecto">Tipo proyecto</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url();?>admin/encargado">Encargados</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url();?>admin/referido">Referidos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url();?>admin/usuariowsp">Consultas Whatsapp</a>
                            </li>                       -->
                    </ul>
                </div>
                <!-- <a href="" style="margin-right: 10px; color: rgba(0,0,0,.5); text-decoration: none;">Cambiar Contraseña</a> -->
                <button class="btn btn-danger btn-logout"><i class="fas fa-sign-out-alt"></i> Logout</button>
            </nav>
        </header><br>

        <script>
        var path = '<?php echo base_url(); ?>';

        /*var tipoUsuario = new Array();
            tipoUsuario = <?php //echo json_encode($tipoUsuario); ?>;*/

        var idUsuario = '<?php echo $idUsuario; ?>';
        var tipo = '<?php echo $tipo; ?>';
        </script>



        <div class="modal" tabindex="-1" role="dialog" id="modalpassword">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Cambiar Contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>encargado/cambiar_pass" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">TIPO DE USUARIO</label>
                            <select class="form-control" name="tipo">
                                <option value="1">Usuario Total</option>
                                <option value="2">Encargado</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombres" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">APELLIDOS</label>
                            <input type="text" name="apellidos" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">DNI</label>
                            <input type="text" name="dni" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">CELULAR</label>
                            <input type="text" name="celular" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">EMAIL</label>
                            <input type="email" name="email" value="" class="form-control" required>
                        </div>
                        <!-- <div class="form-group">
                            <label for="">PROYECTO</label>
                            <select class="form-control" name="proyecto" required>
                                
                            </select>
                        </div> -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>