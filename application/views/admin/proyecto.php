<?php 
    /*$tipoUsuario = $this->session->userdata('tipoUsuario');*/
    $idUsuario = $this->session->userdata('idUsuario');
    $nombreUsuario = $this->session->userdata('nombreUsuario');
 ?>
<style>
    .table th, .table td{
        font-size:12px;
        padding: 5px;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/actividad.css">

<div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Eliminar Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar el proyecto #<span class="id"></span>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger btn-eliminar">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="container" id="proyecto">
    <h1 class="page-title">Proyectos</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" ><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <br>
    <form action="<?php echo base_url(); ?>" method="post">
        <div class="row filtros">
            <div class="col-sm-3">
                <div class="card-box">
                    <label>Cliente</label>
                    <select class="form-control" name="cliente">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
            
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="30">ID</th>
                            <th width="90">Proyecto</th>
                            <th width="90">Cilente</th>
                            <th width="50">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalagregar">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>proyecto/agregar" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">PROYECTO</label>
                            <input type="text" name="nombre" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">CLIENTE</label>
                            <select name="cliente" class="form-control" required>
                            	<option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>proyecto/actualizar" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">ID</label>
                            <input type="text" name="id" value="" class="form-control" readonly>
                        </div>
                         <div class="form-group">
                            <label for="">PROYECTO</label>
                            <input type="text" name="nombre" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">CLIENTE</label>
                            <select name="cliente" class="form-control" required>
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <!--<a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a> -->
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">¡Alerta!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro que desea eliminar este Encargado?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


</div>
