<?php

	class Proyecto_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}

        public function listar($cliente, $where, $order_by){
          
            $sql = "SELECT c.cln_id, c.cln_descripcion, p.pry_descripcion, p.pry_id from proyecto p 
            inner join cliente c on c.cln_id = p.cln_id " . $where . $order_by;

            if($cliente != ""){ $query = $this->db->query($sql, array($cliente)); }
            else{ $query = $this->db->query($sql); }
           
            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

		

        public function agregar($param){
         
          return $this->db->insert('proyecto', $param);

        }

        public function editar($param, $id){
    
            $this->db->where('pry_id', $id);
            $result = $this->db->update('proyecto', $param);

            return $result;
        }


        public function eliminar($pry_id){
            $this->db->where('pry_id', $pry_id);
            $result = $this->db->delete('proyecto');

            return $result;
        }



	}
?>