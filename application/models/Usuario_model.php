<?php
	
	class Usuario_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}

		public function listado(){

			$sql = "SELECT * FROM usuario WHERE usr_estado = 1 ";

			$query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
		}
	}
?>