<?php
	class Actividad_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}


		public function listar($cln_id, $pry_id, $fecha, $where, $order_by){

            $sql = "SELECT act.act_id as 'id', act.usr_id, usr.usr_nombre as 'nombre', 
                    pry.pry_id as 'idproyecto', pry.pry_descripcion as 'proyecto', 
                    c.cln_descripcion as 'cliente', act.act_descripcion as 'tarea', 
                    act.act_tiempo as 'tiempo', act.act_fecha as 'fecha' FROM actividad 
                    as act inner join usuario as usr on act.usr_id = usr.usr_id 
                    inner join proyecto as pry on act.pry_id = pry.pry_id inner join cliente 
                    as c on c.cln_id = pry.cln_id ". $where . $order_by;

            if($cln_id == "" && $pry_id == "" && $fecha == ""){
                 $query = $this->db->query($sql);
            }else{
                if($cln_id != "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $pry_id, $fecha)); }
                if($cln_id != "" && $pry_id == "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id)); }
                if($cln_id != "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id, $pry_id)); }
                if($cln_id != "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $fecha)); }
                if($cln_id == "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($pry_id)); }
                if($cln_id == "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($pry_id, $fecha)); }
                if($cln_id == "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($fecha)); }
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }

            
        }

        

        public function reporteTareas(){

            $this->db->select("usr.usr_nombre as nombre, pry.pry_id as idproyecto, pry.pry_descripcion as proyecto, 
                               c.cln_descripcion as cliente,  act.act_descripcion as tarea, act.act_tiempo as tiempo, act.act_fecha as fecha");
          
            $this->db->from('actividad as act');
            $this->db->join('usuario as usr', 'act.usr_id = usr.usr_id');
            $this->db->join('proyecto as pry', 'act.pry_id = pry.pry_id');
            $this->db->join('cliente as c', 'c.cln_id = pry.cln_id');
            $this->db->order_by("act.act_fecha", "desc");

            return $this->db->get();

        }

        public function reporteTareasPorFiltro($cln_id, $pry_id, $fecha){

            $this->db->select("usr.usr_nombre as nombre, pry.pry_id as idproyecto, pry.pry_descripcion as proyecto, 
                               c.cln_descripcion as cliente,  act.act_descripcion as tarea, act.act_tiempo as tiempo, act.act_fecha as fecha");
          
            $this->db->from('actividad as act');
            $this->db->join('usuario as usr', 'act.usr_id = usr.usr_id');
            $this->db->join('proyecto as pry', 'act.pry_id = pry.pry_id');
            $this->db->join('cliente as c', 'c.cln_id = pry.cln_id');

            if($cln_id != ""){

                $this->db->where('c.cln_id', $cln_id);
            }

            if($pry_id != ""){

                $this->db->where('pry.pry_id', $pry_id);
            }

            if($fecha != ""){
                $this->db->where("SUBSTRING(act.act_fecha, 1,7) = '$fecha'");
            }

            $this->db->order_by("act.act_fecha", "desc");

            return $this->db->get();

        }



		public function agregar($param){

			return $this->db->insert('actividad', $param);
		}

        public function eliminar($id){
            $sql = "delete from actividad where act_id=?";
            $query = $this->db->query($sql,array($id));

        }


        public function editar($id, $param){

            $this->db->where('act_id', $id);
            $result =  $this->db->update('actividad', $param); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2

            return $result;
        }


        public function totalHoras($cln_id, $pry_id, $fecha, $where, $order_by){

            $sql = "SELECT sum(act.act_tiempo) as 'totalHoras' FROM actividad as act 
                    inner join usuario as usr on act.usr_id = usr.usr_id inner join proyecto as pry on 
                    act.pry_id = pry.pry_id inner join cliente c on c.cln_id = pry.cln_id " . $where .$order_by;

            if($cln_id == "" && $pry_id == "" && $fecha == ""){
                 $query = $this->db->query($sql);
            }else{
                if($cln_id != "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $pry_id, $fecha)); }
                if($cln_id != "" && $pry_id == "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id)); }
                if($cln_id != "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id, $pry_id)); }
                if($cln_id != "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $fecha)); }
                if($cln_id == "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($pry_id)); }
                if($cln_id == "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($pry_id, $fecha)); }
                if($cln_id == "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($fecha)); }
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }
	} 
?>