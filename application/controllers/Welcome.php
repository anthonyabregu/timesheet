<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');

		// Get output html
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('pdf');
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as PDF
        $this->dompdf->render();
        
        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
	}

	public function mpdf(){
		//$this->load->view('welcome_message');

		$hoy = date("YmdHis");
		$nameFile = "files/PROPUESTAPICNIC[MENORCA]_" . $hoy . ".pdf";
		
		$mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('welcome_message',[],true);
        $mpdf->WriteHTML($html);
        //$mpdf->Output(); // Abrir en el navegador
        $mpdf->Output($nameFile,'F'); // guardar dentro del proyecto
        //$mpdf->Output('arjun.pdf','D'); // Descargar en el sistema 
	}
}
