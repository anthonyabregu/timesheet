<?php
	
	class Usuario extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model("Usuario_model", "usuario");
		}

		public function listado(){

			$lista = $this->usuario->listado();

			if($lista != false){

				$res["res"] = "ok";
				$res["lista"] = $lista;

			}else{

				$res["res"] = "failed";

			}

			echo json_encode($res);
		}
	}
?>