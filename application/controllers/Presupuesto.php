<?php
	
	class Presupuesto extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model('Presupuesto_model', 'presupuesto');
			$this->load->model('Cliente_model', 'cliente');
		
		}

		public function listar(){

			$cln_id = intval($this->input->get("cliente"));
			$pry_id = intval($this->input->get("proyecto"));
			$fecha = $this->input->get("fecha");

			$order_by = " order by prs.prs_fecha desc ";
			$where = "WHERE prs.prs_id is not null ";

			if($cln_id != ""){ $where = $where . " AND prs.cln_id = ? ";}
			if($pry_id != ""){ $where = $where . " AND prs.pry_id = ? ";}
			if($fecha != ""){ $where = $where . " AND SUBSTRING(prs.prs_fecha, 1,7) = ? "; }

			$lista = $this->presupuesto->listar($cln_id, $pry_id, $fecha, $where, $order_by); 
			/*if($cln_id == "" && $pry_id == "" && $fecha == ""){	
				$lista = $this->actividad->listar(); 
			}else{
				$lista = $this->actividad->listarPorFiltros($cln_id, $pry_id, $fecha);
			}*/

			if($lista != false){
	            $res["res"] = "ok";
	            $res["lista"] = $lista;
	        }else{
	        	$res["res"] = "failed";
	        }

	        echo json_encode($res);
		} 

		public function agregar(){

			$param["prs_descripcion"] = $this->input->post('descripcion');
			$param["prs_detalle"] = $this->input->post('detalle');
			$param["prs_precio"] = floatval($this->input->post('precio'));
			$param["cln_id"] = $this->input->post('cliente');
			$param["pry_id"] = $this->input->post('proyecto');

			$agregar = $this->presupuesto->agregar($param);

			if($agregar != false){

				$cliente = $this->cliente->nombrecliente($param["cln_id"]);

				$pdf = $this->pdf($param["prs_detalle"], $cliente->cln_descripcion);
				$res["res"] = 'ok';

			}else{

				$res["res"] = 'failed';
			}

			echo json_encode($res);
		}


		public function editar(){

			$id = $this->input->post('id');

			$param["prs_descripcion"] = $this->input->post('descripcion');
			$param["prs_detalle"] = $this->input->post('detalle');
			$param["prs_precio"] = floatval($this->input->post('precio'));
			$param["cln_id"] = $this->input->post('cliente');
			$param["pry_id"] = $this->input->post('proyecto');

			$editar = $this->presupuesto->editar($param, $id);

			if($editar != false){

				$res["res"] = 'ok';

			}else{

				$res["res"] = 'failed';
			}

			echo json_encode($res);
		}

		function eliminar($id){
			
			$this->presupuesto->eliminar($id);
			$res["res"] = "ok";
			echo json_encode($res);

		}

		public function pdf($detalle, $cliente){

			$hoy = date("YmdHis");
			$nameFile = "files/PROPUESTAPICNIC[".$cliente."]_" . $hoy . ".pdf";
		
			$mpdf = new \Mpdf\Mpdf();

			$html = $detalle;
	        
	        //$html = $this->load->view('welcome_message',[],true);
	        
	        $mpdf->WriteHTML($html);
	        //$mpdf->Output(); // Abrir en el navegador
	        $mpdf->Output($nameFile,'F'); // guardar dentro del proyecto
	        //$mpdf->Output('arjun.pdf','D'); // Descargar en el sistema 

		}
	}
?>