<?php

  header('Access-Control-Allow-Origin: *');

  use Restserver\Libraries\REST_Controller;
  require APPPATH . '/libraries/REST_Controller.php';

	class Proyecto extends REST_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Proyecto_model', 'proyecto');
		}

		

		public function listar_get(){

          /*if(!$this->session->has_userdata('admin')){
  			       exit();
          }*/

          $cliente = intval($this->input->get("cliente"));

          $order_by = " ORDER BY p.pry_id asc";
          $where = "";

          if($cliente != ""){ $where = $where . " WHERE c.cln_id = ? " ;}

          $lista = $this->proyecto->listar($cliente, $where, $order_by);
          

          if( $lista != false )
          {
            $res["res"] = "ok";
            $res["lista"] = $lista;
          }
          else{
            $res = 'empty';
            
          }

          echo json_encode($res);

    }


		public function agregar(){

          $param['pry_descripcion'] = $this->input->post('nombre');
          $param['cln_id'] = intval($this->input->post('cliente'));

          $result = $this->proyecto->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            echo json_encode($res);
          }
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));

            $param['pry_descripcion'] = $this->input->post('nombre');
            $param['cln_id'] = intval($this->input->post('cliente'));
          	

            $editar = $this->proyecto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		        echo json_encode($res);
            }
        }


        public function eliminar($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->proyecto->eliminar($id);


            $res["res"] = "ok";
            echo json_encode($res);
        }

	}
?>