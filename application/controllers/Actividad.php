<?php
	class Actividad extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model('Actividad_model', 'actividad');
		}

		public function listar(){

			$cln_id = intval($this->input->get("cliente"));
			$pry_id = intval($this->input->get("proyecto"));
			$fecha = $this->input->get("fecha");

			$iduser = $this->input->get("iduser");

			$order_by = " order by act.act_fecha desc ";
			$where = "WHERE usr.usr_estado = 1 ";

			if($cln_id != ""){ $where = $where . " AND c.cln_id = ? ";}
			if($pry_id != ""){ $where = $where . " AND pry.pry_id = ? ";}
			if($fecha != ""){ $where = $where . " AND SUBSTRING(act.act_fecha, 1,7) = ? "; }

			if($iduser != ""){ $where = $where . " AND act.usr_id = $iduser "; }

			$lista = $this->actividad->listar($cln_id, $pry_id, $fecha, $where, $order_by); 
			/*if($cln_id == "" && $pry_id == "" && $fecha == ""){	
				$lista = $this->actividad->listar(); 
			}else{
				$lista = $this->actividad->listarPorFiltros($cln_id, $pry_id, $fecha);
			}*/

			if($lista != false){
	            $res["res"] = "ok";
	            $res["lista"] = $lista;
	        }else{
	        	$res["res"] = "failed";
	        }

	        echo json_encode($res);
		}

		public function agregar(){

			$param["usr_id"] = $this->input->post('usuario');
			$param["pry_id"] = intval($this->input->post("proyecto"));
			$param["act_descripcion"] = $this->input->post("tarea");
			//$param["act_tiempo"] = intval($this->input->post("horas"));
			$param["act_fecha"] = $this->input->post("fecha");

			$agregar = $this->actividad->agregar($param);

			if($agregar != false){
	            $res["res"] = "ok";
	        }else{
	        	$res["res"] = "failed";
	        }

	        echo json_encode($res);
		}

		public function actualizar(){

			$id = intval($this->input->post("id"));
			$param["usr_id"] = intval($this->input->post("usuario"));
			$param["pry_id"] = intval($this->input->post("proyecto"));
			$param["act_descripcion"] = $this->input->post("tarea");
			$param["act_tiempo"] = intval($this->input->post("horas"));
			$param["act_fecha"] = $this->input->post("fecha");

			$editar = $this->actividad->editar($id, $param);

			if($editar != false){
				$res["res"] = "ok"; 
			}else{
				$res["res"] = "failed";
			}

			echo json_encode($res);
		}

		public function registrarhora(){

			$id = intval($this->input->post("id"));
			$param["act_tiempo"] = intval($this->input->post("horas"));
			
			$editar = $this->actividad->editar($id, $param);

			if($editar != false){
				$res["res"] = "ok"; 
			}else{
				$res["res"] = "failed";
			}

			echo json_encode($res);
		}

		function eliminar($id){
			$this->actividad->eliminar($id);
			$res["res"] = "ok";
			echo json_encode($res);
		}

		
		public function reportecsv(){

			//echo "HOLAAA";

			//var_dump("expression");

            $file_name = 'tareas_'.date('Ymd').'.csv'; 
            header("Content-Description: File Transfer"); 
            header("Content-Disposition: attachment; filename=$file_name"); 
            header("Content-Type: application/csv;");

            $cln_id = intval($this->input->post("cliente"));
            $pry_id = intval($this->input->post("proyecto"));
            $fecha = $this->input->post("fecha");

            //echo $file_name . $pry_id . $fecha;

            

            if($cln_id == "" && $pry_id == "" && $fecha == ""){   
                $result = $this->actividad->reporteTareas(); 
            }else{
                $result = $this->actividad->reporteTareasPorFiltro($cln_id, $pry_id, $fecha);
            }

            if($result != false){
               
                // file creation 
                $file = fopen('php://output', 'w');

                $res["export"] = "ok";
                
                $header = array("NOMBRE","IDPROYECTO","PROYECTO","CLIENTE","TAREA","TIEMPO","FECHA"); 
                fputcsv($file, $header);

                foreach ($result->result_array() as $key => $value)
                { 
                    $value["tiempo"] = $value["tiempo"] . 'hora(s)';

                	fputcsv($file, $value); 
                }
                fclose($file); 

            }else{
                $res["export"] = "failed";
            }

            //return $res;
        }


        public function totalHoras(){

        	$cliente = intval($this->input->get("cliente"));
			$proyecto = intval($this->input->get("proyecto"));
			$fecha = $this->input->get("fecha");
			$iduser = $this->input->get("iduser");

			$order_by = " order by act.act_fecha desc ";
			$where = "WHERE usr.usr_estado = 1 ";

			if($cliente != ""){ $where = $where . " AND c.cln_id = ? ";}
			if($proyecto != ""){ $where = $where . " AND pry.pry_id = ? ";}
			if($fecha != ""){ $where = $where . " AND SUBSTRING(act.act_fecha, 1,7) = ? "; }
			if($iduser != ""){ $where = $where . " AND act.usr_id = $iduser "; }

			if($cliente == "" && $proyecto == "" && $fecha == ""){

				$res["res"] = "ok";
				$res["lista"] = "indefinido";

			}else{
				$lista = $this->actividad->totalHoras($cliente, $proyecto, $fecha, $where, $order_by);
	
				if($lista != false){
		            $res["res"] = "ok";
		            $res["lista"] = $lista;
		        }else{
		        	$res["res"] = "failed";
		        }
			}

	        echo json_encode($res);
		}
	}
?>