<?php

	class Exportar_csv extends CI_Controller{

		function __construct(){

			parent::__construct();
			$this->load->model('Actividad_model', 'actividad');
		}


		public function tareas(){

            //$file_name = 'contactos'.date('Ymd').'.csv'; 

            $file_name = 'tareas_'.date('Ymd').'.csv'; 
            header("Content-Description: File Transfer"); 
            header("Content-Disposition: attachment; filename=$file_name"); 
            header("Content-Type: application/csv;");

            $pry_id = intval($this->input->post("proyecto"));
            $fecha = $this->input->post("fecha");

            if($pry_id == "" && $fecha == ""){   
                $result = $this->actividad->listar(); 
            }else{
                $result = $this->actividad->listarPorFiltros($pry_id, $fecha);
            }

            if($result != false){
               
                // file creation 
                $file = fopen('php://output', 'w');

                $res["export"] = "ok";
                
                $header = array("ID", "NOMBRE","IDPROYECTO","PROYECTO","CLIENTE","TAREA","TIEMPO","FECHA"); 
                fputcsv($file, $header);

                foreach ($result->result_array() as $key => $value)
                { 
                    $value["tiempo"] = $value["tiempo"] . 'hora(s)';

                	fputcsv($file, $value); 
                }
                fclose($file); 

            }else{
                $res["export"] = "failed";
            }

            return $res;
        }

	} 
?>