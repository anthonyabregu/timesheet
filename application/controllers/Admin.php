<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model('Administrador_model','admin');
        $this->load->library('session');
        //$this->load->library('email');
    }


    public function index()
    {

        $this->versesion();

         $data = array(
            "page" => "home"
        );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/home');
        $this->load->view('admin/footer');

    }

    private function versesion(){

        if(!$this->session->has_userdata('Conexion')){
            redirect('admin/login');
            exit();
        }
    }

    public function login()
    {
        $this->load->view('admin/login');
    }

    
	public function actividad(){
        $this->versesion();

        $data = array(
                "page" => "actividad"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/actividad');
        $this->load->view('admin/footer');
    }

    public function cliente(){
        $this->versesion();

        $data = array(
                "page" => "cliente"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/cliente');
        $this->load->view('admin/footer');
    }

    public function proyecto(){
        $this->versesion();

        $data = array(
                "page" => "proyecto"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/proyecto');
        $this->load->view('admin/footer');
    }

    public function presupuesto(){
        $this->versesion();

        $data = array(
                "page" => "presupuesto"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/presupuesto');
        $this->load->view('admin/footer');
    }

    public function summernote(){
        $this->versesion();

        $data = array(
                "page" => "summernote"
            );

        $this->load->view('admin/summernote');
    }

}
