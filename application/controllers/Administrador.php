<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Administrador extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model('Administrador_model','admin');
        $this->load->library('session');
        //$this->load->library('email');
    }

    public function login(){

        $usuario = $this->input->post("usuario");
        $clave = $this->input->post("clave");

        $user = $this->admin->login($usuario, $clave);

        if($user != false){

            $s_usuario =  array('id' => $user->usr_id,
                                'nombre' => $user->usr_nombre,
                                'tipo' => $user->usr_tipo,
                                'Conexion' => TRUE);

            //Ponemos en session
            $this->session->set_userdata($s_usuario);

            redirect("admin");

        }else{

            redirect("admin/login?error=error");
        }

    }

    public function logout(){

        $this->session->unset_userdata('id');
        $this->session->unset_userdata('nombre');
        $this->session->unset_userdata('tipo');
        $this->session->unset_userdata('Conexion');
        $this->session->sess_destroy();
        redirect("admin/login");
    }




}
